# Playing Card API

## What is it?

This repository is the home of an API which can be used for any game that includes playing cards. It has several useful features such as pulling out a random card, returning a shuffled deck, and dealing cards out randomly to a specified number of players.

## Dependencies

- Running without Docker
  - Node (I used 20.2.0)
  - NPM (I used 9.6.7)
- Running with Docker
  - Docker

## Running the App

### No Docker Solution

- To run the card api without using Docker, you simple have to open a new terminal in the root of the repository and enter the following commands:

```bash
npm i &&\
npm start
```

### Docker Solution

- Using Docker you can run the card api by using the following commands in a terminal:

```bash
docker pull valatore/cardapi:latest
docker run valatore/cardapi:latest
```

#### Building Docker Images Yourself

If you would like to build the Docker images for yourself from this repo instead of pulling them from my Dockerhub, you can do so by entering the following commands in to a terminal at the root of this repo:

```bash
docker build -t whatever-image-name-you-want:whatever-version-you-want .
```

## Closing the API

If you ran the API with the npm start command, you can close the app by either closing the relevant terminal or by pressing "ctrl + c" in your terminal to execute the API.

If you ran the API using docker, you can close the app by running the following commands:

```bash
docker container ls # run this to find the CONTAINER ID of the card api
docker kill <CONTAINER_ID>
```

## Testing the App

The API has an automated test suite that is built in Jest. It can be run with the following commands:

```bash
npm i
npm test
```

### Endpoints

- Gets all cards and data from the deck

```bash
curl -X GET localhost:3001/cards
```

- Get a random card from the deck and displays the image of the card

```bash
curl -X GET localhost:3001/random
```

- Gets a card by the ID of the card (range is 1-52)

```bash
curl -X GET localhost:3001/card/21
```

- Gets a new deck and shuffles the order of it before returning the result

```bash
curl -X GET localhost:3001/shuffle
```

- Gets a shuffled deck and then deals cards out to a specified number of players until all cards are dealt

```bash
curl -x GET localhost:3001/deal/4
```