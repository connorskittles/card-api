import app from './index.js';

const hostname = 'localhost';
const port = 3001;

app().listen(port, function (err) {
  if (err) console.log(err);
  console.log(`Server listening on ${hostname}:${port}`);
});
