import { cards } from '../config/cards.js';

export const shuffleCards = () => {
  let newDeck = Array.from(cards);
  let originalIndex = cards.length,
    randomIndex;

  while (originalIndex != 0) {
    randomIndex = Math.floor(Math.random() * originalIndex);
    originalIndex--;

    [newDeck[originalIndex], newDeck[randomIndex]] = [
      newDeck[randomIndex],
      newDeck[originalIndex],
    ];
  }
  return newDeck;
};

export const getRandomCard = () => {
  let number = Math.ceil(Math.random() * cards.length);
  return number.toString();
};

export const getCardById = (key) => {
  for (let card in cards)
    if (cards[card].id == key) return cards[card].id;
    else if (key >= 53 || key <= 0 || cards[card].id > key) return 'invalid';
};

export const getAllCards = () => {
  return cards;
};

export const createPlayers = (numOfPlayers) => {
  let players = [];

  for (let i = 0; i < numOfPlayers; i++) {
    let player = {
      playerName: `Player ${i + 1}`,
      id: i,
      cards: [],
    };
    players.push(player);
  }
  return players;
};

export const dealCards = (players) => {
  const newDeck = shuffleCards();
  const participants = createPlayers(players);

  while (newDeck.length >= 1) {
    for (let i = 0; i < players; i++) {
      newDeck.length >= 1
        ? (participants[i].cards = participants[i].cards.concat(newDeck.pop()))
        : console.log('out of cards');
    }
  }
  return participants;
};
