import request from 'supertest';
import app from '../index.js';
import cards from '../config/cards.js';

const res = request(app());

describe('Test API Calls', () => {
  it('should return a random card between 1 and 52', async () => {
    await res.get('/random').expect(200).expect('Content-Type', 'image/png');
  });

  it('should return a card image with given id', async () => {
    await res.get('/card/21').expect(200).expect('Content-Type', 'image/png');
  });

  it('should return an image of invalid with an id out of range', async () => {
    await res.get('/card/53').expect(200).expect('Content-Type', 'image/png');
  });

  it('should return all cards', async () => {
    await res.get('/cards').expect(200).expect('Content-Type', /json/);
    expect(res.text).toBe(cards);
  });

  it('should return shuffled deck', async () => {
    await res
      .get('/shuffle')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect('Content-Length', '2825');
  });

  it('should deal out cards to players ', async () => {
    await res.get('/deal/4').expect(200).expect('Content-Type', /json/);
  });

  it('should deal out cards to players and expect some players to have less cards if player count not divisible by 52', async () => {
    await res
      .get('/deal/10')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect('Content-Length', '3256');
  });

  it('should return 404 for unknown url', async () => {
    await res.get('/bad-link').expect(404);
  });
});
